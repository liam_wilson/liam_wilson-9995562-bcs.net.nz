﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Assessment_2___9995562
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public int Choice1 = 0;
        public int Choice2 = 0;
        public string FromAddress = "";
        public string MessageText = "";
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            FromAddress = FromTextBox.Text;
            MessageText = MessageTextBox.Text;
            Choice1 = ToBox.SelectedIndex;
            Choice2 = SubjectBox.SelectedIndex;

            MessageDialog CheckMessage = new MessageDialog($"From: {FromAddress}\nSubject: {SubjectChoice(Choice2)}\nMessage:\n\n{MessageText}");
            CheckMessage.Title = "Are you happy with your message?";

            CheckMessage.Commands.Add(new Windows.UI.Popups.UICommand("Send") { Id = 0 });
            CheckMessage.Commands.Add(new Windows.UI.Popups.UICommand("Cancel") { Id = 1 });
            CheckMessage.Commands.Add(new Windows.UI.Popups.UICommand("Reset") { Id = 2 });

            CheckMessage.DefaultCommandIndex = 0;
            CheckMessage.CancelCommandIndex = 1;

            var result = await CheckMessage.ShowAsync();

            if ((int)result.Id == 0)
            {
                Application.Current.Exit();
            }
            else if ((int)result.Id == 2)
            {
                ToBox.SelectedIndex = -1;
                FromTextBox.Text = String.Empty;
                SubjectBox.SelectedIndex = -1;
                MessageTextBox.Text = string.Empty;
            }
        }

        static string SubjectChoice(int Choice2)
        {
            var SecondChoice = "";

            switch (Choice2)
            {
                case 0:
                    SecondChoice = "Low Priority";
                    break;
                case 1:
                    SecondChoice = "Medium Priority";
                    break;
                case 2:
                    SecondChoice = "High Priority";
                    break;
            }
            return SecondChoice;
        }

    }
}